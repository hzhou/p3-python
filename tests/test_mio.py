# Ejercicio 4
# Ejercicios 9

import unittest
from mysound import Sound


class PruebaSound(unittest.TestCase):
    def test_duracion(self):
        sound = Sound(2)  # Crear un sonido de 2 segundos
        self.assertEqual(2, sound.duration)


if __name__ == '__main__':
    unittest.main()

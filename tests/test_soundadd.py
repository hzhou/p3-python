# Ejercicio 8
# Ejercicio 9

import unittest


from soundops import soundadd
from mysound import Sound


class PruebaSoundAdd(unittest.TestCase):
    def test_soundadd_mismo_length(self):
        s1 = Sound(3)
        s1.buffer = [1, 2, 3]
        s2 = Sound(3)
        s2.buffer = [4, 5, 6]
        resultado_esperado = Sound(3)
        resultado_esperado.buffer = [5, 7, 9]
        resultado = soundadd(s1, s2)
        self.assertEqual(resultado.duration, resultado_esperado.duration)
        self.assertEqual(resultado.buffer, resultado_esperado.buffer)

    def test_soundadd_diferente_length(self):
        s1 = Sound(5)
        s1.buffer = [1, 2, 3, 4, 5]
        s2 = Sound(3)
        s2.buffer = [6, 7, 8]
        resultado_esperado = Sound(3)
        resultado_esperado.buffer = [7, 9, 11]
        resultado = soundadd(s1, s2)
        self.assertEqual(resultado.duration, resultado_esperado.duration)
        self.assertEqual(resultado.buffer, resultado_esperado.buffer)

    def test_soundadd_vacio(self):
        s1 = Sound(0)
        s1.buffer = []
        s2 = Sound(3)
        s2.buffer = [1, 2, 3]
        resultado_esperado = Sound(0)
        resultado_esperado.buffer = []
        resultado = soundadd(s1, s2)
        self.assertEqual(resultado.duration, resultado_esperado.duration)
        self.assertEqual(resultado.buffer, resultado_esperado.buffer)


if __name__ == '__main__':
    unittest.main()

import unittest
from soundops import soundadd
from mysound import Sound

class TestSoundSin(unittest.TestCase):
    def test_mio(self):
        s1 = Sound(3)
        s1.buffer = [1, 2, 3]
        s2 = Sound(3)
        s2.buffer = [4, 5, 6]
        s3 = soundadd(s1, s2)

        self.assertEqual(len(s1.buffer), len(s2.buffer))
        self.assertEqual(s3.buffer[2], s1.buffer[2] + s2.buffer[2])
        self.assertEqual(len(s3.buffer), len(s1.buffer))
        self.assertEqual(len(s3.buffer), len(s2.buffer))
